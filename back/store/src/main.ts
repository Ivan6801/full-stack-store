import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Configuración de CORS
  const corsOptions: CorsOptions = {
    origin: '*', // o puedes especificar los orígenes permitidos aquí
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // métodos permitidos
    allowedHeaders: ['Content-Type', 'Authorization'], // encabezados permitidos
    preflightContinue: false,
    optionsSuccessStatus: 204,
  };

  app.useGlobalPipes(new ValidationPipe());
  app.enableCors(corsOptions); // habilitar CORS con las opciones definidas
  await app.listen(3000);
}
bootstrap();
