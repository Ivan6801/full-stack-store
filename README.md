# full-stack-store JavaScript and TypeScript



~~~bash  
const MyName = "Ivan";
const MyAge = 24;
const suma = (a: number, b: number) => a + b;

class Person {
  constructor(private age: number, private name: string) {}

  getSummary() {
    return `I'm ${this.name} and I'm ${this.age}`;
  }
}

const ivan = new Person(24, "Ivan");
console.log(ivan.getSummary());
~~~

## token key: glpat-x2DbHdFHeNAjja6LNUj2